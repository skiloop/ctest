
#include<stdio.h>
#include<string.h>

#ifndef _WIN32
#include <sys/mman.h>
#include <sys/types.h>
#include <dirent.h>
#if __linux__ && __GLIBC__ || __APPLE__
# include <sys/sysctl.h>
#endif
#include <unistd.h>
#endif


#if defined(_WIN32)
	char * getappname()
	{
		char fn[MAX_PATH] = {0};
		GetModuleFileNameA(0, fn, MAX_PATH);
		char * pch = fn + strlen(fn);
		while (pch > fn && !(pch[-1] == '\\' || pch[-1] == '/')) --pch;
		memmove(fn, pch, strlen(pch)+1);
		pch = strrchr(fn, '.');
		if (pch) *pch = 0;
		return _strdup(fn);
	}
#elif  defined(__APPLE__)
	char * getappname()
	{
		struct kinfo_proc process_info;
		size_t process_info_len = sizeof(process_info);
		int process_info_mib[4] = { CTL_KERN, KERN_PROC, KERN_PROC_PID, getpid() };
		int process_info_mib_len = 4;
		memset(&process_info, 0, sizeof(process_info));
		if (sysctl(process_info_mib, process_info_mib_len, &process_info, &process_info_len, NULL, 0) == 0)
		{
			return strdup(process_info.kp_proc.p_comm);
		}

		return strdup("unknown app");
	}
#elif defined(__linux__)
	char * getappname()
	{
		char path[1024] = {0};
		char id[256];
		sprintf(id, "/proc/%d/exe", getpid());
		if (readlink(id, path, sizeof(path)-1) <= 0) path[0]=0;
		const char * pf = strrchr(path, '/');
		if (pf) pf++;
		else pf = path;
		if (pf[0] == 0) pf = "unkapp";
		return strdup(pf);
	}
#endif

int main(){
    printf("%s\n",getappname());
}

