/*************************************************************************
	> File Name: intbin.c
	> Author: 
	> Mail: 
	> Created Time: 二  2/14 18:06:04 2017
 ************************************************************************/

#include<stdio.h>
#include<stdlib.h>

int main(int argc, const char **argv) {
    if (argc <= 1) {
        printf("usage: %s int1 int2 ... intn\n", argv[0]);
        return 0;
    }
    FILE *fp = fopen("int.bin", "wb");
    for (int i = 1; i < argc; i++) {
        long c = strtol(argv[i], NULL, 10);
        fwrite(&c, sizeof(int), 1, fp);
        printf("%ld\n", c);
    }
    fclose(fp);
    return 0;
}
