//
//  main.c
//  src
//
//  Created by Baofeng Shii on 2017/1/5.
//  Copyright © 2017年 Baofeng Shii. All rights reserved.
//

#include <stdio.h>
#include<inttypes.h>

struct{
    int n;
    long d;
    int c;
} st;
struct{
    long d;
    int n;
    int c;
} ss;
struct{
    int n;
    int c;
    long d;
} s1;
struct{
    uint64_t l;
    uint32_t n;
    volatile int c;
    uint64_t d;
} s2;

int main(){
    printf("size of char: %lu\n",sizeof(char));
    printf("size of uint32_t: %lu\n",sizeof(uint32_t));
    printf("size of uint64_t: %lu\n",sizeof(uint64_t));
    printf("size of int: %lu\n",sizeof(int));
    printf("size of float: %lu\n",sizeof(float));
    printf("size of double: %lu\n",sizeof(double));
    printf("size of long: %lu\n",sizeof(long));
    printf("size of st: %lu\n",sizeof(st));
    printf("size of ss: %lu\n",sizeof(ss));
    printf("size of s1: %lu\n",sizeof(s1));
    printf("size of s2: %lu\n",sizeof(s2));
    s2.l=1;
    s2.n=1;
    s2.c=-9999;
    s2.d=17;
    // write struct
    FILE *fp=fopen("struct.bin","wb");
    fwrite(&s2,sizeof(s2),1,fp);
    fclose(fp);
    return 0;
}
