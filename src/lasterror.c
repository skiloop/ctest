
#include<stdio.h>
#include<errno.h>
#include<string.h>

#ifndef _WIN32
#include <sys/mman.h>
#include <sys/types.h>
#include <dirent.h>
#if __linux__ && __GLIBC__ || __APPLE__
# include <sys/sysctl.h>
#endif
#include <unistd.h>
#endif


	char * getlasterrstr(char * msg, int msglen)
	{
#ifdef _WIN32
		DWORD err = GetLastError();
		LPTSTR lpMsgBuf = 0;
		FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL, err, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
			(LPTSTR) &lpMsgBuf, 0, NULL );
		string sa;
		sa.resize(wcslen(lpMsgBuf)*3+10);
		int n = WideCharToMultiByte(CP_UTF8, 0, lpMsgBuf, -1, &sa[0], (int)sa.length(), 0, 0);
		if (n <= 0) sa.clear();
		else sa.resize(n);
		LocalFree(lpMsgBuf);
		sutil::strlcpy(msg, sa.c_str(), msglen);
		return msg;
#elif defined(__linux__)
#if (_POSIX_C_SOURCE >= 200112L || _XOPEN_SOURCE >= 600) && ! _GNU_SOURCE
		strerror_r(errno, msg, msglen);
		return msg;
#else
        return strerror_r(errno, msg, msglen);
#endif
#else
        char * s = strerror(errno);
        strlcpy(msg, s, msglen);
        return msg;
#endif
	}


int main(){
#ifdef __linux__
#if (_POSIX_C_SOURCE >= 200112L || _XOPEN_SOURCE >= 600) && ! _GNU_SOURCE
    printf("return int\n");
#else
    printf("return string\n");
#endif
#endif
    FILE *fp=fopen("/tmp/hello.oosdw","r");

    if (fp == 0){
        char err[100];
        err[99]=0;
        char *ep=getlasterrstr(err,99);
        printf("error: %s\n",ep);
    }else{
        fclose(fp);
        printf("success");
    }
    return 0;
}