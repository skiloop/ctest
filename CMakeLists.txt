cmake_minimum_required(VERSION 3.1)

project(ctests VERSION 1.0.0 DESCRIPTION "c/cpp program tests")

enable_testing()


add_executable(getappname src/getappname.c)
add_executable(fmt src/fmt.c)
add_executable(lasterror src/lasterror.c)
add_executable(writeb src/writeb.c)
add_executable(intbin src/intbin.c)
add_executable(c_size src/c_size.c)
add_executable(cpp_size src/cpp_size.cpp)

add_subdirectory(shared)

