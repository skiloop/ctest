#include<iostream>
using namespace std;

int main(){
	int z=10;
	char c='z';
	int *p=0;
	float f=10.0;
	unsigned int u=10;
	uint64_t i64;
	uint32_t i32;
	cout<<"size of int: "<<sizeof(z)<<endl;
	cout<<"size of char: "<<sizeof(c)<<endl;
	cout<<"size of int ptr: "<<sizeof(p)<<endl;
	cout<<"size of float: "<<sizeof(f)<<endl;
	cout<<"size of unsigned int: "<<sizeof(u)<<endl;
	cout<<"size of unsigned int32: "<<sizeof(i32)<<endl;
	cout<<"size of unsigned int64: "<<sizeof(i64)<<endl;
	return 0;
}
